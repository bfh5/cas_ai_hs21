
# Aufgabe:
# Update des Target-Modells mit den Gewichten des Online-Modells -> umbauen von fix auf soft-update 0.1

from gym.envs import box2d
from tqdm import tqdm
import torch 
import torch.nn as nn 
import torch.nn.functional as F 
import torch.optim as optim 
from torch.distributions import Categorical 

import numpy as np 
import gym 
import random 

from model import DuelingNetwork, BranchingQNetwork
from utils import TensorEnv, ExperienceReplayMemory, AgentConfig, BranchingTensorEnv
import utils

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

#torch.manual_seed(1)
#np.random.seed(1)


class BranchingDQN(nn.Module): 

    def __init__(self, obs, ac, n, config):                                                                             # 4.1

        super().__init__()

        self.q = BranchingQNetwork(obs, ac,n )                                                                          # 4.2 Online Netzwerk
        self.target = BranchingQNetwork(obs, ac,n )                                                                     # 4.3 Target Netzwerk

        self.target.load_state_dict(self.q.state_dict())                                                                # Laden des Online-Modells in das Target-Modell

        self.target_net_update_freq = config.target_net_update_freq                                                     # Target-Modell Update-Häufigkeit
        self.update_counter = 0                                                                                         # Zähler wie häufig ein Update des Online-Modells erfolgt ist

    def get_action(self, x):                                                                                            # 9.1

        with torch.no_grad(): 
            # a = self.q(x).max(1)[1]
            out = self.q(x).squeeze(0)                                                                                  # 9.2
            action = torch.argmax(out, dim = 1)  # argmax über die spalten
        return action.numpy()

    def update_policy(self, adam, memory, params):                                                                      # 10.1

        b_states, b_actions, b_rewards, b_next_states, b_masks = memory.sample(params.batch_size)                       # auslesen des replay buffers und Aufteilung der Datenobjekte

        states = torch.tensor(b_states).float()                                                                         # von einer Liste in einen Tensor umgewandelt = Tensor(2,24) (hier batch-Grösse 2)
        actions = torch.tensor(b_actions).long().reshape(states.shape[0],-1,1)                                          # von zwei Arrays in einer Liste in einen Tensor umgewandelt und eine Dimension hinzugefügt = Tensor(2,4,1)
        rewards = torch.tensor(b_rewards).float().reshape(-1,1)                                                         # von einer Liste in einen Tensor umgewandelt und eine Dimension hinzugefügt = Tensor(2,1)
        next_states = torch.tensor(b_next_states).float()                                                               # von einer Liste in einen Tensor umgewandelt = Tensor(2,24)
        masks = torch.tensor(b_masks).float().reshape(-1,1)                                                             # done entweder 0 oder 1 (0=done, 1!=done), von einer Liste in einen Tensor umgewandelt, plus 1 Dimension hinzugefügt = Tensor(2,24)
                                                                                                                        # PrintScreen 7
        qvals = self.q(states)                                                                                          # Test-Übergabe der states an das Online-Netzwerk, als Rückgabe die q-values = Tensor(batchGr,4,6)

        current_q_values = self.q(states).gather(2, actions).squeeze(-1)                                                # 10.2  auslesen der qvalues, in der 3. Dim. mit Index von actions, und letzte Dimension löschen = Tensor(batchGr,4) = eine q-value pro action
                                                                                                                        # gather(2, actions) - Mit gather und der Angabe von Dimension und Index können in einem Tensor gezielt Elemente ausgelesen werden
                                                                                                                        # squeeze(-1) - die letzte Dimension wird gelöscht
                                                                                                                        # PrintScreen 9
        with torch.no_grad():

            argmax = torch.argmax(self.q(next_states), dim = 2)                                                         # 10.3 Online-Modell, Abfrage mit den next_states-Daten, Rückgabe der Indizes der next_q_value-Maximalwerte
            #argmax = torch.argmax(self.target(next_states), dim=2)                                                     # torch.argmax gibt den Index des Maximalwerts zurück, Tensor(batchGr,4) = tensor([[0, 0, 4, 4],[4, 0, 1, 4]])

            max_next_q_vals = self.target(next_states).gather(2, argmax.unsqueeze(2)).squeeze(-1)                       # 10.4 Target-Modell, Abfrage mit den next_states-Daten und den obigen Indizes, Rückgabe der next_q_value-Maximalwerte = tensor([[0.1294, 0.1836, 0.1172, 0.1372], [0.1116, 0.1409, 0.0891, 0.1343]])
            #max_next_q_vals = max_next_q_vals.mean(1, keepdim = True)                                                  # Änderung wegen Warning-Meldung
            max_next_q_vals = max_next_q_vals.mean(1, keepdim = True).expand(-1, max_next_q_vals.shape[1])              # Einbau von expand, damit mean mehrmals ausgeführt wird (hier 4x); Rückgabe der Mittelwerte = tensor([[0.1418, 0.1418, 0.1418, 0.1418], [0.1190, 0.1190, 0.1190, 0.1190]])

        expected_q_vals = rewards + max_next_q_vals*0.99*masks                                                          # Berechnung von expected_q_values = tensor([[0.0185, 0.0185, 0.0185, 0.0185], [0.0808, 0.0808, 0.0808, 0.0808]]); 0.99 ist hier hart codiert = Gamma
        # print(expected_q_vals[:5])
        loss = F.mse_loss(expected_q_vals, current_q_values)                                                            # Berechnung von loss aus exüected/current_q_values

        # input(loss)

        # print('\n'*5)
        
        adam.zero_grad()                                                                                                # PyTorch zero_grad
        loss.backward()                                                                                                 # PyTorch backward

        for p in self.q.parameters():                                                                                   # Nach dem Backward ändern sich die Gradienten. Wenn diese zu hoch geraten sind, werden diese hier auf den Bereich 1 bis -1 begrenzt
            p.grad.data.clamp_(-1.,1.)

        adam.step()                                                                                                     # PyTorch step

        # Update des Target-Modells mit den Gewichten des Online-Modells -> umbauen auf soft_update 0.1
        self.update_counter += 1
        if self.update_counter % self.target_net_update_freq == 0:                                                      # Wenn das Online-Modell eine definierte Anzahl Updates erfahren hat, wird auch beim Target-Modell ein Update ausgeführt
            self.update_counter = 0 
            #self.target.load_state_dict(self.q.state_dict())                                                           # Variante 1: die Gewichte des Target-Modells werden komplett mit den Gewichten des Online-Modells überschrieben
                                                                                                                        # Variante 2: soft_update von Target-Modell: die Gewichte des Target-Modell werden nur teilweise mit Gewichten des Online-Modells überschrieben
            tau = 0.3  # orig 0.1                                                                                       # z.B. 90% der Gewichte von Target- und 10% von Online-Modell -> tau = 0.1
            for target_param, q_param in zip(self.target.parameters(), self.q.parameters()):
                target_param.data.copy_(tau * q_param.data + (1.0 - tau) * target_param.data)








if __name__ == "__main__":

    args = utils.arguments()

    bins = 6                                                                                                           # Anzahl Intervalle für die action-Diskretisierung
    env = BranchingTensorEnv(args.env, bins)                                                                            # 1.  Erzeugung von env und Erstellung Diskretisierungsintervalle
    #env.seed(1)
                                                                                                                        # PrintScreen 1
    config = AgentConfig()                                                                                              # 2.  Parameter für des Agenten initialisieren
    memory = ExperienceReplayMemory(config.memory_size)                                                                 # 3.  Replay Buffer initialisieren
    agent = BranchingDQN(env.observation_space.shape[0], env.action_space.shape[0], bins, config)                       # 4.  Initialisierung der beiden Netzwerke; Anzahl states = 24, Anzahl actions = 4, bins/Intervalle = 6, config = Konfigurationsparameter des Agenten
    adam = optim.Adam(agent.q.parameters(), lr = config.lr)                                                             # Initialisierung des Optimizers

    s = env.reset()                                                                                                     # 5.  Env reset = Initialisierung von 24 states
    ep_reward = 0.                                                                                                      # Variable für reward-Summe je Epoche
    recap = []                                                                                                          # Variable für die Speicherung des summierten rewards je Epoche
    #z_done_counter = 0

    p_bar = tqdm(total = config.max_frames)                                                                             # PrintScreen 3
    for frame in range(config.max_frames):

        epsilon = config.epsilon_by_frame(frame)                                                                        # 6.  Exponentielle Methode für die epsilon Reduzierung, epsilon=1.0 bei frame=0
        #print("Frame und Epsilon: ", frame, ", ", epsilon)

        if np.random.random() > epsilon:                                                                                # Wenn epsilon grösser ist, werden die vier actions gesampelt,
            action = agent.get_action(s)                                                                                # 9,  wenn epsilon kleiner ist, werden die actions im Netzwerk abgefragt
        else:
            action = np.random.randint(0, bins, size = env.action_space.shape[0])                                       # vier gesampelte Zahlen von 0 bis 5, welche als Index für den Intervallzugriff verwendet werden; array mit vier Elementen

        ns, r, done, infos = env.step(action)                                                                           # 8.  step mit den vier actions aufrufen, Rückgabe von next-state, reward, done und infos
        ep_reward += r                                                                                                  # Summierung des rewards
        #reward_positiv = 0
        #if r > 0:
        #    reward_positiv = 1
        #else:
        #    reward_positiv = 0
                                                                                                                        # PrintScreen 5
        if done:                                                                                                        # wenn done:
            ns = env.reset()                                                                                            # die next-states werden mit neu initialiserten states überschrieben
            recap.append(ep_reward)                                                                                     # der summierte reward von dieser Epoche wird gespeichert
            p_bar.set_description('Rew: {:.3f}'.format(ep_reward))                                                      # Ausgabe des summierten rewards von dieser Epoche
            ep_reward = 0.
            #z_done_counter += 1
            #print("done: ", z_done_counter, ", frame: ", frame, ", memory: ", len(memory))

        memory.push((s.reshape(-1).numpy().tolist(), action, r, ns.reshape(-1).numpy().tolist(), 0. if done else 1.))   # 7.  Speicherung in den replay buffer: aktueller state (als flache Liste), action, reward, next-state (als flache Liste) und done=0/1
        s = ns                                                                                                          # die aktuellen states werden mit den next-states überschrieben

        p_bar.update(1)                                                                                                 # Ausgabe progress bar

        if frame > config.learning_starts:                                                                              # das Lernen des Netzwerks wird ab dem definierten frame-Stand ausgeführt
            agent.update_policy(adam, memory, config)                                                                   # 10.  übergabe des Optimizers, des replay buffers und der Konfigurationsparameter

        if frame % 1000 == 0:                                                                                           # periodische Speicherung des Modells und den Ergebnissen
            utils.save(agent, recap, args)



    p_bar.close()


    """
    ### Description
    This is a simple 4-joint walker robot environment.
    There are two versions:
    - Normal, with slightly uneven terrain.
    - Hardcore, with ladders, stumps, pitfalls.
    To solve the normal version, you need to get 300 points in 1600 time steps.
    To solve the hardcore version, you need 300 points in 2000 time steps.
    A heuristic is provided for testing. It's also useful to get demonstrations
    to learn from. To run the heuristic:
    ```
    python gym/envs/box2d/bipedal_walker.py
    ```
    
    ### Action Space
    Actions are motor speed values in the [-1, 1] range for each of the
    4 joints at both hips and knees.
    
    ### Observation Space
    State consists of hull angle speed, angular velocity, horizontal speed,
    vertical speed, position of joints and joints angular speed, legs contact
    with ground, and 10 lidar rangefinder measurements. There are no coordinates
    in the state vector.
    
    ### Rewards
    Reward is given for moving forward, totaling 300+ points up to the far end.
    If the robot falls, it gets -100. Applying motor torque costs a small
    amount of points. A more optimal agent will get a better score.
    
    ### Starting State
    The walker starts standing at the left end of the terrain with the hull
    horizontal, and both legs in the same position with a slight knee angle.
    
    ### Episode Termination
    The episode will terminate if the hull gets in contact with the ground or
    if the walker exceeds the right end of the terrain length.
    
    ### Arguments
    To use to the _hardcore_ environment, you need to specify the
    `hardcore=True` argument like below:
    ```python
    import gym
    env = gym.make("BipedalWalker-v3", hardcore=True)
    ```
    
    ### Version History
    - v3: returns closest lidar trace instead of furthest;
        faster video recording
    - v2: Count energy spent
    - v1: Legs now report contact with ground; motors have higher torque and
        speed; ground has higher friction; lidar rendered less nervously.
    - v0: Initial version
 
    """