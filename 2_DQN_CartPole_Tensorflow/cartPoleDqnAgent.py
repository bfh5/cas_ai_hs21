import collections
import os
import random
from typing import Deque

import gym
import numpy as np

from cartPoleDqn import DQN


# Pfad für die Speicherung der Modelle
PROJECT_PATH = os.path.abspath("C:/Users/Roger/Documents/BFH/bfh_Kurs_AI/2022.02.01/DQN")
MODELS_PATH = os.path.join(PROJECT_PATH, "models")
MODEL_PATH = os.path.join(MODELS_PATH, "dqn_cartpole.h5")
TARGET_MODEL_PATH = os.path.join(MODELS_PATH, "target_dqn_cartpole.h5")


class Agent:
    def __init__(self, env: gym.Env):                                                                                   # Agent bekommt das env übergeben und liest folgende Daten aus:
        # DQN Env Variables                                                                                             # Definition von observations und actions
        self.env = env
        self.observations = self.env.observation_space.shape                                                            # CartPole = 4
        self.actions = self.env.action_space.n    # CartPole = 2

        # DQN Agent Variables                                                                                           # Initialisierung der Variablen des Agenten
        self.replay_buffer_size = 50_000                                                                                # orig 50_000, besser 100_000
        self.train_start = 8                                                                                            # nach soviel observations startet er das training, orig 1_000
                                                                                                                        # besser 10_000
        self.memory: Deque = collections.deque(                                                                         # Deque ist eine Queue mit der Grösse von reply buffer size
            maxlen=self.replay_buffer_size                                                                              # eine Deque für den Buffer bewirkt eine schnellere Verarbeitung
        )
        self.gamma = 0.95                                                                                               # orig 0.95, Diskretisierungsparameter, getestet 0.99
        self.epsilon = 1.0                                                                                              # orig 1.0, Startpunkt
        self.epsilon_min = 0.01                                                                                         # orig 0.01, Endpunkt, 0.1 auch möglich, damit am Ende auch noch
                                                                                                                        # etwas gesampelt wird, getestet 0.35
        self.epsilon_decay = 0.999                                                                                      # orig 0.995, kann auch vergrössert werden, damit er langsamer
                                                                                                                        # abfallen solld, getestet 0.9995

        # DQN Network Variables
        self.state_shape = self.observations                                                                            # Definition von states aus observations, CartPole = 4
        self.learning_rate = 0.001                                                                                      # orig 1e-3 / 0.001, getestet 0.00025
        self.dqn = DQN(                                                                                                 # Erzeugung des Online-Modells
            self.state_shape,
            self.actions,
            self.learning_rate
        )
        self.target_dqn = DQN(                                                                                          # Erzeugung des Target-Modells (mit den gleichen Parametern)
            self.state_shape,
            self.actions,
            self.learning_rate
        )
        self.batch_size = 8                                                                                             # orig 32, oder 64/128

    def get_action(self, state: np.ndarray):                                                                            # epsilon greedy Methode
        if np.random.rand() <= self.epsilon:                                                                            # wenn epsilon grösser als eine random Nummer zwischen 0/1 ist
            return np.random.randint(self.actions)                                                                      # wird die action gesampelt
        else:
            return np.argmax(self.dqn(state))                                                                           # wenn epsilon kleiner ist, holt er sich die Aktion aus dem
                                                                                                                        # Online-Netzwerk
                                                                                                                        # d.h. je kleiner das epsilon, desto grösser ist die
                                                                                                                        # Wahrscheinlichkeit, dass er die action aus dem Netzwerk holt

    def train(self, num_episodes: int):                                                                                 # zentrale Methode
        last_rewards: Deque = collections.deque(maxlen=5)                                                               # Queue zur Speicherung der letzten fünf total_reward, aus
        best_reward_mean = 0.0                                                                                          # welchen jeweils der Durchschnitt berechnet und zum Vergleich
                                                                                                                        # mit best_reward_mean verwendet wird

        for episode in range(1, num_episodes + 1):                                                                      # Training über die Anzahl Episoden
            total_reward = 0.0                                                                                          # Initialisierung von total_reward
            state = self.env.reset()                                                                                    # innerhalb der Episode holt man sich den ersten state mit reset()
            state = np.reshape(state, newshape=(1, -1)).astype(np.float32)                                              # state reshape um Format [1,4] als float
                                                                                                                        # zu erhalten (1 Zeile mit 4 Spalten)

            while True:                                                                                                 # Iteration innerhalb der Episode solange True ist, d.h.
                                                                                                                        # wenn best_reward_mean ein bestimmter Wert erreicht hat (= Ziel, gewonnen) wird
                                                                                                                        # die gesamte Verarbeitung beendet (return),
                                                                                                                        # wenn done=True ist (er ist umgekippt) wird die aktuelle Episode beendet (break),
                                                                                                                        # ansonsten wird weiter iteriert
                action = self.get_action(state)                                                                         # holen der action mit Übergabe des aktuellen state
                next_state, reward, done, _ = self.env.step(action)                                                     # step mit Übergabe der action und Erhalt von
                                                                                                                        # nächsten state, reward und ob done erreicht
                next_state = np.reshape(next_state, newshape=(1, -1)).astype(np.float32)                                # reshape nächster state [1,4]
                #print("Step: ", state, ", ", action, ", ", reward, ", ", done, ", ", next_state)

                                                                                                                        # wenn umgekippt und das Ziel von 500 erreicht ist -> gut
                                                                                                                        # wenn umgekippt und das Ziel von 500 nicht erreicht ist -> schlecht
                if done and total_reward < 500:                                                                         # wenn done=True (=umgekippt) und total_reward < 500, orig 499
                    reward = -100.0                                                                                     # wird eine grosse Reduktion beim reward durchgeführt,
                                                                                                                        # Erklärung: damit soll das Verständnis verstärkt werden, dass das
                                                                                                                        # was gemacht wurde, zum umkippen des cartpole geführt hat, d.h.
                                                                                                                        # nur wenn er umgefallen ist und das Ziel von 500 nicht erreicht ist,
                                                                                                                        # wird der reward zur Strafe verstärkt belastet,
                                                                                                                        # dies ist wichtig um das Training zu verbessern

                self.remember(state, action, reward, next_state, done)                                                  # schreibt die Ergebnisse in den Buffer und
                                                                                                                        # es wird auch noch das epsilon reduziert
                self.replay()                                                                                           # hier erfolgt nun das eigentliche Training
                total_reward += reward                                                                                  # total_reward wird mit dem reward inkrementiert
                state = next_state                                                                                      # der aktuelle state wird mit dem nächsten state überschrieben

                if done:                                                                                                # wenn umgekippt
                    if total_reward < 500:                                                                              # wenn das Ziel nicht erreicht ist, wird die obige Strafreduktion
                        total_reward += 100.0                                                                           # (für das Training) wieder rückgängig gemacht, danit hier
                                                                                                                        # für die Statistik wieder der korrekte reward ausgewiesen wird

                    #self.target_dqn.update_model(self.dqn)                                                             # Alternative Stelle für Update des Target-Modells:
                                                                                                                        # wenn nach jedem done, d.h. somit nach jedem Ende einer
                                                                                                                        # Episode das Update des Target-Modells erfolgen soll,
                                                                                                                        # dies soll das Training verbessern!?

                    print(f"Episode: {episode} Reward: {total_reward} Epsilon: {self.epsilon}")
                    last_rewards.append(total_reward)                                                                   # der aktuelle total_reward wird in die Queue der letzten
                                                                                                                        # fünf total_rewards aufgenommen
                    current_reward_mean = np.mean(last_rewards)                                                         # Durchschnitt der neuen fünf letzten total_rewards,
                                                                                                                        # Anpassung auf 10 oder 20 möglich
                    #print("rewards: ", total_reward, ", ", last_rewards, ", ", current_reward_mean)

                    if current_reward_mean > best_reward_mean:                                                          # wenn neuer Durchschnitt grösser als bisheriger best
                        self.target_dqn.update_model(self.dqn)                                                          # Hier erfolgt der Update das Target-Modells nur,
                                                                                                                        # wenn der Durchschnitt verbessert worden ist.
                        best_reward_mean = current_reward_mean                                                          # neuer best_reward_mean (Update)
                        self.dqn.save_model(MODEL_PATH)                                                                 # Speicherung Online-Modell
                        self.target_dqn.save_model(TARGET_MODEL_PATH)                                                   # # Speicherung Target-Modell
                        print(f"New best mean: {best_reward_mean}")

                        if best_reward_mean > 450:                                                                      # orig. 400, wenn bestimmer Durchschnitt des total_rewards
                            return                                                                                      # erreicht ist (Ziel), wird die gesamte Verarbeitung beendet
                    break                                                                                               # wenn done wird die aktuelle Episode beendet

        print("len_self.memory: ", len(self.memory))

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))                                                   # Speicherung der Ergebnisse in den Buffer
        if self.epsilon > self.epsilon_min:                                                                             # wenn epsilon noch nicht den definierten Mindestwert hat,
            self.epsilon *= self.epsilon_decay                                                                          # erfolgt die Reduktion des epsilon gemäss definiertem decay Wert

    def replay(self):                                                                                                   # zentrale Methode für das Modell-Training
        # print("memory und train_start: ", len(self.memory), ", ", self.train_start)

        if len(self.memory) < self.train_start:                                                                         # das Training wird nicht ausgeführt, wenn die Anzahl definierten
                                                                                                                        # Einträge im buffer noch nicht erreicht sind!
            self.epsilon = 1.0                                                                                          # Einbau, damit erst beim Trainieren epsilon reduziert wird
            return

        minibatch = random.sample(self.memory, self.batch_size)                                                         # er sampelt aus dem buffer
        states, actions, rewards, states_next, dones = zip(*minibatch)                                                  # erzeugt daraus je Objekt eine Liste

        states = np.concatenate(states).astype(np.float32)                                                              # verkettet die states zu einem np.array
        states_next = np.concatenate(states_next).astype(np.float32)                                                    # verkettet die states_next zu einem np.array

        q_values = self.dqn(states)                                                                                     # er holt sich die q_values aus dem Online-Modell
        q_values_next = self.target_dqn(states_next)                                                                    # er holt sich die q_values_next aus dem Target-Modell

        for i in range(self.batch_size):                                                                                # Training im loop innerhalb eines Batches
            a = actions[i]
            done = dones[i]
            if done:
                q_values[i][a] = rewards[i]                                                                             # wenn done ist das q_value zu dieser action genau reward
            else:                                                                                                       # wenn noch nicht done, ist das q_value zu dieser action:
                                                                                                                        # der reward plus gamma * das Maximum von q_value_next
                q_values[i][a] = rewards[i] + self.gamma * np.max(q_values_next[i])

        self.dqn.fit(states, q_values)                                                                                  # beim fit sind die states der Input und die q_values die Labels/Targets

    def play(self, num_episodes: int, render: bool = True):
        self.dqn.load_model(MODEL_PATH)
        self.target_dqn.load_model(TARGET_MODEL_PATH)

        for episode in range(1, num_episodes + 1):
            total_reward = 0.0
            state = self.env.reset()
            state = np.reshape(state, newshape=(1, -1)).astype(np.float32)

            while True:
                if render:
                    self.env.render()
                action = self.get_action(state)
                next_state, reward, done, _ = self.env.step(action)
                next_state = np.reshape(next_state, newshape=(1, -1)).astype(np.float32)
                total_reward += reward
                state = next_state

                if done:
                    print(f"Episode: {episode} Reward: {total_reward}")
                    break




if __name__ == "__main__":
    seed = 1
    np.random.seed(seed)
    random.seed(seed)
    env = gym.make("CartPole-v1")
    env.seed(seed)
    agent = Agent(env)
    agent.train(num_episodes=150)   # orig 250, getestet 3300
    input("Play?")
    agent.play(num_episodes=20, render=True)
