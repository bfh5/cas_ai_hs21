import numpy as np
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam

#tf.random.set_seed(seed)


# tensorflow-Klasse für DQN Modell
# Mit dieser Klasse werden zwei tensorflow-Modelle erstellt: Online-Modell und Target-Modell
# Trainiert wird nur das Online-Modell.
# Im Target-Modell werden periodisch die Gewichte des Online-Modells übernommen.
# Wenn die Klasse aufgerufen wird, wird das Modell als Property erstellt.
# Mit den Klassen-Methoden wird das Modell bearbeitet.

class DQN(Model):
    def __init__(self, state_shape: int, num_actions: int, learning_rate: float):
        super().__init__()
        self.state_shape = state_shape                                                                                  # state Definition, mit CartPole = 4 Parameter
        self.num_actions = num_actions                                                                                  # action Definition, mit CartPole = 2 Parameter
        self.learning_rate = learning_rate                                                                              # learning rate
        self.internal_model = self.build_model()                                                                        # Erzeugung des Modells als Property

    def build_model(self) -> Model:                                                                                     # Netzwerk spezifisch tensorflow
        input_state = Input(shape=self.state_shape)                                                                     # Anzahl states als Input
        x = Dense(units=24)(input_state)                                                                                # orig 24, Dense-Methode mit Angabe des Input-Parameters
        x = Activation("relu")(x)
        x = Dense(units=24)(x)                                                                                          # orig 24
        x = Activation("relu")(x)
        q_value_pred = Dense(self.num_actions)(x)                                                                       # Output: Anzahl actions = Anzahl Q-Values, CartPole = 2
        model = Model(                                                                                                  # Erstellung des Modells mit Input- und Output-Definitionen
            inputs=input_state,
            outputs=q_value_pred
        )
        model.compile(                                                                                                  # Kompilierung des Modells mit Angabe von loss und optimizer
            loss="mse",
            optimizer=Adam(learning_rate=self.learning_rate)
        )
        return model

    def call(self, inputs: np.ndarray) -> np.ndarray:                                                                   # In dieser Methode werden die Inputs dem Modell übergeben
        return self.internal_model(inputs).numpy()                                                                      # und die Q-Values zurückgegeben

    def fit(self, states: np.ndarray, q_values: np.ndarray):                                                            # Methode zum Trainieren des Modells,
                                                                                                                        # in PyTorch: zero grad, model input, loss, backward, step
        self.internal_model.fit(x=states, y=q_values, verbose=0)

    def update_model(self, other_model: Model):                                                                         # Methode für den Update der Gewichte des Modells basierend auf den
                                                                                                                        # Gewichten eines anderen Modells:
                                                                                                                        # Target-Modell wird mit den Online-Modell-Gewichten überschrieben
        self.internal_model.set_weights(other_model.get_weights())

    def load_model(self, path: str):                                                                                    # Laden des Modells
        self.internal_model.load_weights(path)

    def save_model(self, path: str):                                                                                    # Speicherung des Modells
        self.internal_model.save_weights(path)


if __name__ == "__main__":                                                                                              # als Test kann hiermit das summary des erzeugten Modells ausgegeben werden
    dqn = DQN(
        state_shape=4,
        num_actions=2,
        learning_rate=0.001
    )
    dqn.internal_model.summary()
