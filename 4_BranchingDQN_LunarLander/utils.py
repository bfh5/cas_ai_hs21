import numpy as np 
import gym 
import torch 
import random
from argparse import ArgumentParser 
import os 
import pandas as pd 

import matplotlib.pyplot as plt 
plt.style.use('ggplot')
from scipy.ndimage.filters import gaussian_filter1d

torch.manual_seed(1)
np.random.seed(1)
random.seed(1)

def arguments(): 

    parser = ArgumentParser()
    parser.add_argument('--env', default = 'LunarLanderContinuous-v2')

    return parser.parse_args()


def save(agent, rewards, args): 

    path = './runs/{}/'.format(args.env)
    try: 
        os.makedirs(path)
    except: 
        pass 

    torch.save(agent.q.state_dict(), os.path.join(path, 'model_state_dict'))

    plt.cla()
    plt.plot(rewards, c = 'r', alpha = 0.3)
    plt.plot(gaussian_filter1d(rewards, sigma = 5), c = 'r', label = 'Rewards')
    plt.xlabel('Episodes')
    plt.ylabel('Cumulative reward')
    plt.title('Branching DDQN: {}'.format(args.env))
    plt.savefig(os.path.join(path, 'reward.png'))

    pd.DataFrame(rewards, columns = ['Reward']).to_csv(os.path.join(path, 'rewards.csv'), index = False)





class AgentConfig:

    def __init__(self,                                                                                                  # 2.1
                 epsilon_start = 1.,                                                                                    # orig 1.0
                 epsilon_final = 0.01,
                 epsilon_decay = 800,                                                                                   # orig 8000, getestet mit 800
                 gamma = 0.99, 
                 lr = 1e-4, 
                 target_net_update_freq = 1000,                                                                         # orig 1000
                 memory_size = 100000, 
                 batch_size = 128,                                                                                      # orig 128, getestet mit 32
                 learning_starts = 128,                                                                                 # orig 5000, getestet mit 128, 32
                 max_frames = 500000):                                                                                  # orig 10000000

        self.epsilon_start = epsilon_start
        self.epsilon_final = epsilon_final
        self.epsilon_decay = epsilon_decay

        self.epsilon_by_frame = lambda i: self.epsilon_final + (self.epsilon_start - self.epsilon_final) * np.exp(-1. * i / self.epsilon_decay)  # 6.1 Exponentielle Methode für die epsilon Reduzierung

        self.gamma =gamma
        self.lr =lr

        self.target_net_update_freq =target_net_update_freq
        self.memory_size =memory_size
        self.batch_size =batch_size

        self.learning_starts = learning_starts
        self.max_frames = max_frames


class ExperienceReplayMemory:
    def __init__(self, capacity):                                                                                       # 3.1
        self.capacity = capacity
        self.memory = []

    def push(self, transition):                                                                                         # 7.1
        self.memory.append(transition)
        if len(self.memory) > self.capacity:                                                                            # wenn der buffer voll ist, wird der älteste Eintrag gelöscht
            del self.memory[0]

    def sample(self, batch_size):                                                                                       # 10.1.1
        
        batch = random.sample(self.memory, batch_size)                                                                  # Erstellung von einem batch mit gesampelten Datensätzen aus dem replay buffer
        states = []
        actions = []
        rewards = []
        next_states = [] 
        dones = []

        for b in batch:                                                                                                 # die Daten der einzelenen Datensätze im batch werden nach Objekt aufgeteilt und in die jeweilige Liste gespeichert
            states.append(b[0])
            actions.append(b[1])
            rewards.append(b[2])
            next_states.append(b[3])
            dones.append(b[4])

        return states, actions, rewards, next_states, dones                                                             # PrintScreen 6

    def __len__(self):
        return len(self.memory)


class TensorEnv(gym.Wrapper): 

    def __init__(self, env_name):                                                                                       # 1.2.1

        super().__init__(gym.make(env_name))

    def process(self, x):                                                                                               # 5.1.1, 8.2.2.1, dem übergebenen array mit dem state wird eine benötigte Dimension hinzugefügt

        return torch.tensor(x).reshape(1,-1).float()

    def reset(self):                                                                                                    # 5.1

        return self.process(super().reset())

    def step(self, a):                                                                                                  # 8.2.1  Übergabe der vier actions zur Verarbeitung des nächsten Steps

        ns, r, done, infos = super().step(a)                                                                            # 8.2.2  PrintScreen 4 mit Rückgabe der next-state, reward, done und infos
        return self.process(ns), r, done, infos 


class BranchingTensorEnv(TensorEnv):

    def __init__(self, env_name, n):                                                                                    # 1.1

        super().__init__(env_name)                                                                                      # 1.2 Erzeugung von env
        self.n = n                                                                                                      # n = 6
        #self.discretized = np.linspace(-1.,1., self.n)                                                                 # für Diskretisierung der actions werden 6 Intervalle erzeugt, hier mit hartcodierten low/high-Angaben
        self.discretized = np.linspace(self.action_space.low[0], self.action_space.high[0], self.n)                     # Variante mit Angabe von low/high aus dem env, Ergebnis = [-1, -0.6, -0.2, 0.2, 0.6, 1]
                                                                                                                        # aus continuous actions werden so diskretisierte actions

    def step(self, a):                                                                                                  # 8.1  für vier actions wird der Intervall-Index übergeben

        action = np.array([self.discretized[aa] for aa in a])                                                           # 8.2  aus continuous actions werden hier diskretisierte actions gemäss Intervalldefinition


        return super().step(action)
