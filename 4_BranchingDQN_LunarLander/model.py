import torch 
import torch.nn as nn 
import torch.nn.functional as F 
import torch.optim as optim 
from torch.distributions import Categorical 

# from torchviz import make_dot
# from torchsummary import summary

torch.manual_seed(1)

class DuelingNetwork(nn.Module): 

    def __init__(self, obs, ac): 

        super().__init__()

        self.model = nn.Sequential(nn.Linear(obs, 128), 
                                   nn.ReLU(), 
                                   nn.Linear(128,128), 
                                   nn.ReLU())

        self.value_head = nn.Linear(128, 1)                                                                             # orig 128, 1
        self.adv_head = nn.Linear(128, ac)

    def forward(self, x): 

        out = self.model(x)

        value = self.value_head(out)
        adv = self.adv_head(out)

        q_val = value + adv - adv.mean(1).reshape(-1,1)                                                                 # state-value plus advantage-values ergeben q-values
        return q_val


class BranchingQNetwork(nn.Module):

    def __init__(self, obs, ac_dim, n):                                                                                 # 4.2.1 / 4.3.1

        super().__init__()

        self.ac_dim = ac_dim                                                                                            # Anzahl actions = 4
        self.n = n                                                                                                      # Anzahl Intervalle = 6

        self.model = nn.Sequential(nn.Linear(obs, 128),                                                                 # Anzahl observations = 24
                                   nn.ReLU(),
                                   nn.Linear(128,128), 
                                   nn.ReLU())

        self.value_head = nn.Linear(128, 1)                                                                             # state-value, als Output 1 node
        self.adv_heads = nn.ModuleList([nn.Linear(128, n) for i in range(ac_dim)])                                      # advantage-value
        ##print("self.value_head: ", self.value_head)                                                                     # PrintScreen 2
        ##print("self.adv_heads: ", self.adv_heads)

    def forward(self, x):                                                                                               # 9.2.1, 10.2.1, 10.3.1, 10.4.1

        out = self.model(x)                                                                                             # Modellausführung: x = (batchGr, 24), out = (batchGr, 128)
        value = self.value_head(out)                                                                                    # value = (batchGr, 1)
        advs = torch.stack([l(out) for l in self.adv_heads], dim = 1)                                                   # adv = (batchGr, 4, 6)

        ##print("out:   ", out)
        ##print("value: ", value)
        ##print("advs:  ", advs)
        # print(advs.shape)
        # print(advs.mean(2).shape)
        test =  advs.mean(2, keepdim = True)                                                                            # test = (batchGr, 4, 1), mean von den sechs Werten zu jeder action
        test2 = value.unsqueeze(2)                                                                                      # test2 = (batchGr, 1, 1) - zusätzliche Dimension
        # input(test.shape)
        q_val = value.unsqueeze(2) + advs - advs.mean(2, keepdim = True )                                               # q-val = (batchGr, 4, 6)
        # input(q_val.shape)

        return q_val                                                                                                    # PrintScreen 8


# PyTorch Modell plotten

#if __name__ == "__main__":
#    b = BranchingQNetwork(5, 4, 6)
#    print(b)
#    summary(b, (5, 4, 6))   # package torchsummary


# b = BranchingQNetwork(5, 4, 6)

# b(torch.rand(10, 5))