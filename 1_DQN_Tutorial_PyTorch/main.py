from collections import deque, namedtuple
from typing import Tuple, OrderedDict, List

import gym
import numpy as np
import torch
from IPython import display
from matplotlib import pyplot as plt
from pytorch_lightning import LightningModule, Trainer
from pytorch_lightning.utilities import DistributedType
from torch import nn, Tensor
from torch.optim import Adam, Optimizer
from torch.utils.data import IterableDataset, DataLoader


class DQN(nn.Module):
    """Simple MLP network."""                                                                                           # Netzwerk anstelle von Q-Table -> lernfähige Q-Table

    def __init__(self, obs_size: int, n_actions: int, hidden_size: int = 128):
        """
        Args:
            obs_size: observation/state size of the environment
            n_actions: number of discrete actions available in the environment
            hidden_size: size of hidden layers
        """
        super().__init__()
        self.net = nn.Sequential(
            nn.Linear(obs_size, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, n_actions),
        )

    def forward(self, x):
        return self.net(x.float())


# Named tuple for storing experience steps gathered in training
Experience = namedtuple(
    "Experience",
    field_names=["state", "action", "reward", "done", "new_state"],                                                     # alles wird im Buffer gespeichert
)


class ReplayBuffer:
    """Replay Buffer for storing past experiences allowing the agent to learn from them.

    Args:
        capacity: size of the buffer
    """

    def __init__(self, capacity: int) -> None:
        self.buffer = deque(maxlen=capacity)                                                                            # Wichtig: wie gross die Queue sein soll! Ilja nimmt normalerweise 100'000

    def __len__(self) -> None:
        return len(self.buffer)

    def append(self, experience: Experience) -> None:
        """Add experience to the buffer.

        Args:
            experience: tuple (state, action, reward, done, new_state)
        """
        self.buffer.append(experience)

    def sample(self, batch_size: int) -> Tuple:  # Sampling zum trainieren
        indices = np.random.choice(len(self.buffer), batch_size, replace=False)
        states, actions, rewards, dones, next_states = zip(
            *(self.buffer[idx] for idx in indices))  # sehr schöner/interessanter Code

        return (
            np.array(states),
            np.array(actions),
            np.array(rewards, dtype=np.float32),
            np.array(dones, dtype=np.bool),
            np.array(next_states),
        )



class RLDataset(IterableDataset):
    """Iterable Dataset containing the ExperienceBuffer which will be updated with new experiences during training.

    Args:
        buffer: replay buffer
        sample_size: number of experiences to sample at a time
    """

    def __init__(self, buffer: ReplayBuffer, sample_size: int = 200) -> None:
        self.buffer = buffer
        self.sample_size = sample_size

    def __iter__(self):                                                                                                 # Orig:  def __iter__(self) -> Tuple:
        states, actions, rewards, dones, new_states = self.buffer.sample(self.sample_size)
        for i in range(len(dones)):                                                                                     # es gibt immer ein Wert in dones, aber die Länge sollte bei allen enthaltenen Variablen gleich sein!?
            yield states[i], actions[i], rewards[i], dones[i], new_states[i]



class Agent:
    """Base Agent class handeling the interaction with the environment."""

    def __init__(self, env: gym.Env, replay_buffer: ReplayBuffer) -> None:
        """
        Args:
            env: training environment
            replay_buffer: replay buffer storing experiences
        """
        self.env = env
        self.replay_buffer = replay_buffer
        self.reset()
        self.state = self.env.reset()

    def reset(self) -> None:
        """Resets the environment and updates the state."""
        self.state = self.env.reset()

    def get_action(self, net: nn.Module, epsilon: float, device: str) -> int:
        """Using the given network, decide what action to carry out using an epsilon-greedy policy.

        Args:
            net: DQN network
            epsilon: value to determine likelihood of taking a random action
            device: current device

        Returns:
            action
        """
        if np.random.random() < epsilon:                                                                                # wenn epsilon noch gross ist, wird die Aktion gesampelt
            action = self.env.action_space.sample()
        else:                                                                                                           # wenn epsilon klein ist, wird nicht mehr gesampelt,
                                                                                                                        # sondern die Aktion mit dem gelernten Netzwerk ermittelt
            state = torch.tensor([self.state])

            if device not in ["cpu"]:
                state = state.cuda(device)

            q_values = net(state)
            _, action = torch.max(q_values, dim=1)                                                                      # es wird die Aktion genommen, welche mit dem grössten Q-Value korrespondiert
            action = int(action.item())

        return action

    @torch.no_grad()
    def play_step(
        self,
        net: nn.Module,
        epsilon: float = 0.0,
        device: str = "cpu",
    ) -> Tuple[float, bool]:
        """Carries out a single interaction step between the agent and the environment.

        Args:
            net: DQN network
            epsilon: value to determine likelihood of taking a random action
            device: current device

        Returns:
            reward, done
        """

        action = self.get_action(net, epsilon, device)                                                                  # nächste Aktion abrufen

        # do step in the environment
        new_state, reward, done, _ = self.env.step(action)                                                              # Step mit der neuen Aktion ausführen

        exp = Experience(self.state, action, reward, done, new_state)                                                   # Experience-Objekt erzeugen

        self.replay_buffer.append(exp)                                                                                  # die neue Experience wird in den Buffer geschrieben, dies Ausführung hier ist zu hinterfragen
                                                                                                                        # besser wäre die Experience zurückzugeben und anschliessend ausserhalb über die Speicherung zu entscheiden

        self.state = new_state                                                                                          # Wechsel auf den nächsten Zustand
        if done:
            self.reset()
                                                                                                                        #return reward, done    für Plot mit action ergänzt
        return action, reward, done




class DQNLightning(LightningModule):
    """Basic DQN Model."""

    def __init__(
        self,
        batch_size: int = 16,                                                                                           # Anzahl Steps, die er sampled, z.B. 32
        lr: float = 1e-2,                                                                                               # Learning-Rate
        env: str = "CartPole-v0",
        gamma: float = 0.99,
        sync_rate: int = 10,                                                                                            # alle 10 Steps, Übergabe der Gewichte vom Online-Network an das zweite Netzwerk zur Erzeugung der Target-Q-Values
        replay_size: int = 1000,                                                                                        # Buffergrösse, Ilja: 100'000
        warm_start_size: int = 1000,                                                                                    # ab 1000 beginnt er zu trainieren, Ilja: 100'000
        eps_last_frame: int = 1000,                                                                                     # allenfalls auch grösser Ilja: 100'000?
        eps_start: float = 1.0,
        eps_end: float = 0.01,
        episode_length: int = 200,                                                                                      # zuerst 200, dann auf 450
        warm_start_steps: int = 1000,
    ) -> None:
        """
        Args:
            batch_size: size of the batches")
            lr: learning rate
            env: gym environment tag
            gamma: discount factor
            sync_rate: how many frames do we update the target network
            replay_size: capacity of the replay buffer
            warm_start_size: how many samples do we use to fill our buffer at the start of training
            eps_last_frame: what frame should epsilon stop decaying
            eps_start: starting value of epsilon
            eps_end: final value of epsilon
            episode_length: max length of an episode
            warm_start_steps: max episode reward in the environment
        """
        super().__init__()
        self.save_hyperparameters()                                                                                     # speichert alle Hyperparameter (Variablen in der init-Methode)

        self.env = gym.make(self.hparams.env)                                                                           # holt Environment
        obs_size = self.env.observation_space.shape[0]                                                                  # holt Observation Size aus der Länge von observation_space
        n_actions = self.env.action_space.n                                                                             # Number of actions

        self.net = DQN(obs_size, n_actions)                                                                             # online network, dieses wird trainiert
        self.target_net = DQN(obs_size, n_actions)                                                                      # target network

        self.buffer = ReplayBuffer(self.hparams.replay_size)
        self.agent = Agent(self.env, self.buffer)
        self.total_reward = 0
        self.episode_reward = 0
        self.populate(self.hparams.warm_start_steps)

    def populate(self, steps: int = 1000) -> None:
        """Carries out several random steps through the environment to initially fill up the replay buffer with
        experiences.

        Args:
            steps: number of random steps to populate the buffer with
        """
        for i in range(steps):
            self.agent.play_step(self.net, epsilon=1.0)                                                                 # hier wird play_step aufgerufen und
                                                                                                                        # hier könnte auch das Schreiben in den Buffer implementiert werden

    def forward(self, x: Tensor) -> Tensor:
        """Passes in a state x through the network and gets the q_values of each action as an output.

        Args:
            x: environment state

        Returns:
            q values
        """
        output = self.net(x)
        return output      # Rückgabe der Q-Values (Q-Value je Aktion)

    def dqn_mse_loss(self, batch: Tuple[Tensor, Tensor]) -> Tensor:
        """Calculates the mse loss using a mini batch from the replay buffer.

        Args:
            batch: current mini batch of replay data

        Returns:
            loss
        """
        states, actions, rewards, dones, next_states = batch

        state_action_values = self.net(states).gather(1, actions.type(torch.int64).unsqueeze(-1)).squeeze(-1)           # er holt sich Q-Values für den aktuellen State
                                                                                                                        # current q-value; Art wie die Werte ausgelesen werden

        with torch.no_grad():
            next_state_values = self.target_net(next_states).max(1)[0]
            next_state_values[dones] = 0.0
            next_state_values = next_state_values.detach()

        expected_state_action_values = next_state_values * self.hparams.gamma + rewards

        return nn.MSELoss()(state_action_values, expected_state_action_values)                                          # die Differenz der aktuellen und erwarteten Values soll kleiner werden

    def training_step(self, batch: Tuple[Tensor, Tensor], nb_batch) -> OrderedDict:
        """Carries out a single step through the environment to update the replay buffer. Then calculates loss
        based on the minibatch recieved.

        Args:
            batch: current mini batch of replay data
            nb_batch: batch number

        Returns:
            Training loss and log metrics
        """
        # Andere decay Varianten ausprobieren !!!
        device = self.get_device(batch)
        epsilon = max(
            self.hparams.eps_end,
            self.hparams.eps_start - self.global_step + 1 / self.hparams.eps_last_frame,
        )

        # step through environment with agent
        ## reward, done = self.agent.play_step(self.net, epsilon, device)  für Plot mit ,_ (Action) ergänzt
        _, reward, done = self.agent.play_step(self.net, epsilon, device)
        self.episode_reward += reward

        # calculates training loss
        loss = self.dqn_mse_loss(batch)

        # mehrere GPU Training
        if self.trainer._distrib_type in {DistributedType.DP, DistributedType.DDP2}:
            loss = loss.unsqueeze(0)

        if done:
            self.total_reward = self.episode_reward
            self.episode_reward = 0

        # Soft update of target network
        if self.global_step % self.hparams.sync_rate == 0:                                                              # sync rate varieren
            self.target_net.load_state_dict(self.net.state_dict())

        log = {
            "total_reward": torch.tensor(self.total_reward).to(device),
            "reward": torch.tensor(reward).to(device),
            "train_loss": loss,
        }
        status = {
            "steps": torch.tensor(self.global_step).to(device),
            "total_reward": torch.tensor(self.total_reward).to(device),
        }

        return OrderedDict({"loss": loss, "log": log, "progress_bar": status})

    def configure_optimizers(self) -> List[Optimizer]:
        """Initialize Adam optimizer."""
        optimizer = Adam(self.net.parameters(), lr=self.hparams.lr)
        return [optimizer]

    def __dataloader(self) -> DataLoader:
        """Initialize the Replay Buffer dataset used for retrieving experiences."""
        dataset = RLDataset(self.buffer, self.hparams.episode_length)
        dataloader = DataLoader(
            dataset=dataset,
            batch_size=self.hparams.batch_size,
        )
        return dataloader

    def train_dataloader(self) -> DataLoader:
        """Get train loader."""
        return self.__dataloader()

    def get_device(self, batch) -> str:
        """Retrieve device currently being used by minibatch."""
        return batch[0].device.index if self.on_gpu else "cpu"








if __name__ == "__main__":

    vh1 = "Hallo"
    vh2 = "Duda"
    vh3 = 4*3
    model = DQNLightning()

    trainer = Trainer(
        # gpus="CPU",
        max_epochs=500,  # orig 200
        val_check_interval=100,
    )

    trainer.fit(model)

    state = model.agent.env.reset()
    ##img = plt.imshow(model.agent.env.render(mode='rgb_array'))
    for t in range(1000):
        # step through environment with agent
        action, _, _ = model.agent.play_step(model.net, 0, 'cpu')
        # action = env.action_space.sample()
        ##img.set_data(model.agent.env.render(mode='rgb_array'))
        ##plt.axis('off')
        ##display.display(plt.gcf())
        ###display.clear_output(wait=True)
        state, reward, done, _ = model.agent.env.step(action)
        if done:
            print('Score: ', t + 1)
            break

    model.agent.env.close()


